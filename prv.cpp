#include <fstream>
#include <iostream>
#include <string>
#include <stdio.h>

#define NUM_LIN_DISTANCIAS 55
#define NUM_COL_DISTANCIAS 3
#define NUM_LIN_PEDIDOS 10
#define NUM_COL_PEDIDOS 4
#define NUM_LIN_VEICULOS 10
#define NUM_COL_VEICULOS 2
#define NUM_LIN_PRODUTOS 10
#define NUM_COL_PRODUTOS 2
#define NUM_DISTANCIA_MAX 1000
#define NUM_QUANT_VEICULOS 20
#define NUM_LIN_LOCAIS 12
#define NUM_COL_LOCAIS 2


using namespace std;


class ManipulaArquivo
{
public:

    ManipulaArquivo(void) //método construtor
     {
   	 //
     }
    ~ManipulaArquivo(void) //Destrutor. Chamado quando o objeto é liberado
     {
      //
    }

    int gravaArquivo (string nome_arquivo_saida)
     {
	   	ofstream outFile (nome_arquivo_saida.c_str( ) );
		if(!outFile ) {
			cerr << "Não foi possivel abrir o arquivo de saida : " <<
			nome_arquivo_saida  << " Saindo do programa! \n";
			return -1;
		}

     }

  int leMatriArquivo (string nome_arquivo_pedidos, int mat_distancia [][NUM_COL_DISTANCIAS],
  int mat_pedido [][NUM_COL_PEDIDOS],int mat_veiculo[][NUM_COL_VEICULOS],string mat_produto[][NUM_COL_PRODUTOS],
                     string mat_local[][NUM_COL_VEICULOS]
                      ,int lin_dist,  int col_dist, int lin_ped,  int col_ped,int lin_veic,
					   int col_veic, int lin_prod, int col_prod,int lin_loc, int col_loc){
  	   // pesquisa https://www.youtube.com/watch?v=m52IvkpI748
       ifstream infile;
       int i=0,j=0;


        /*Le arquivos das distancias */

       infile.open("distancias.txt");
	   if (infile.is_open() && infile.good()) //verificamos se está tudo bem
	    {
	    infile >> lin_dist >> col_dist;  //le a ordem da matriz no arquivo

	     for (i=0;i<lin_dist;i++){
	     	for (j=0;j<col_dist;j++){
	     		infile >> mat_distancia[i][j];  //lê cada item da matriz
			 }
		 }
	    infile.close();
	    }

	    /*Le arquivos dos pedidos */

       infile.open("pedidos.txt");
	   if (infile.is_open() && infile.good()) //verificamos se está tudo bem
	    {
	    infile >> lin_ped >> col_ped;  //le a ordem da matriz no arquivo
	     //cout << "  lin: "<< lin_ped << " col: \n"<<  col_ped;
	     for (i=0;i<lin_ped;i++){
	     	for (j=0;j<col_ped;j++){
	     		infile >> mat_pedido[i][j];  //lê cada item da matriz
			 }
		 }
	    infile.close();
	    }

		/*Le arquivos dos veiculos */

       infile.open("veiculos.txt");
	   if (infile.is_open() && infile.good()) //verificamos se está tudo bem
	    {
	    infile >> lin_veic >> col_veic;  //le a ordem da matriz no arquivo

	     for (i=0;i<lin_veic;i++){
	     	for (j=0;j<col_veic;j++){
	     		infile >> mat_veiculo[i][j];  //lê cada item da matriz
			 }
		 }
	    infile.close();
	    }


       	/*Le arquivos dos veiculos */

       infile.open("produtos.txt");
	   if (infile.is_open() && infile.good()) //verificamos se está tudo bem
	    {
	    infile >> lin_prod >> col_prod;  //le a ordem da matriz no arquivo


	     for (i=0;i<lin_prod;i++){
	     	for (j=0;j<col_prod;j++){
	     		infile >> mat_produto[i][j];  //lê cada item da matriz
			 }
		 }
	    infile.close();
	    }

	    infile.open("locais.txt");
	   if (infile.is_open() && infile.good()) //verificamos se está tudo bem
	    {
	    infile >> lin_loc >> col_loc;  //le a ordem da matriz no arquivo


	     for (i=0;i<lin_loc;i++){
	     	for (j=0;j<col_loc;j++){
	     		infile >> mat_local[i][j];  //lê cada item da matriz
			 }
		 }
	     infile.close();
	    }


	 //   system ("pause");
       return 1;

   }

};

class Veiculo {
	private:
	int numero;
	string tipo;
	int capacidade;

	public:
		Veiculo(void){
			//
		}
		~Veiculo(void){
			//
		}
	 /*Getters e Setters*/
     void setNumero(int num) {
          numero= num;
     }
     int getNumero() {
      return numero;
     }
     void setTipo(string tip) {
          tipo= tip;
     }
     string getTipo() {
      return tipo;
     }
     void setCapacidade(int capac) {
          capacidade= capac;
     }
     int getCapacidade() {
      return capacidade;
     }
};

class Local {
	private:
	int numero;
	string descricao;

	public:
		Local(void){
			//
		}
		~Local(void){
			//
		}
	 /*Getters e Setters*/
     void setNumero(int num) {
          numero= num;
     }
     int getNumero() {
      return numero;
     }
     //
     void setDescricao(string desc) {
          descricao= desc;
     }
     string getDescricao() {
      return descricao;
     }

};

class Distancia {
	private:
	Local origem;
	Local destino;
	int valor;

	public:
	 Distancia(void)	{

	 }
	 ~Distancia(void)	{

	 }

	 /*Getters e Setters*/
	 void setOrigem(Local orig) {
          origem= orig;
     }
     Local getOrigem() {
      return origem;
     }
     void setDestino(Local dest) {
          destino= dest;
     }
     Local getDestino() {
      return destino;
     }
     void setValor(int val) {
          valor = val;
     }
     int  getValor() {
      return valor;
     }

};

class Pedido {
	private:
	int numero;
	int quantidade;
	Local local;
	Veiculo veiculo;
	public:
		Pedido(void){
			//
		}
		~Pedido(void){
			//
		}
	 /*Getters e Setters*/
     void setNumero(int num) {
          numero= num;
     }
     int getNumero() {
      return numero;
     }
     void setQuantidade(int quant) {
          quantidade= quant;
     }
     int getQuantidade() {
      return quantidade;
     }
     void setLocal(Local loc) {
          local= loc;
     }
     Local getLocal() {
      return local;
     }
     void setVeiculo(Veiculo veic) {
          veiculo= veic;
     }
     Veiculo getVeiculo() {
      return veiculo;
     }

};



class ManipulaArray{

public:
	ManipulaArray(void) //método construtor
    {
      	//
    }
    ~ManipulaArray(void) //Destrutor. Chamado quando o objeto é liberado
     {
      //
     }

   int* zeraMatriz(int mat[][NUM_COL_DISTANCIAS], int numLinha, int numColuna)
   {
    int a,b;
       for (a=0;a<numLinha;a++){
       	for (b=0;b<numColuna;b++){
             mat[a][b]=0;
     }
    }
    return(*mat);
    }
     int* zeraMatrizRotas(int mat[][NUM_LIN_DISTANCIAS], int numLinha, int numColuna)
   {
    int a,b;
       for (a=0;a<numLinha;a++){
       	for (b=0;b<numColuna;b++){
             mat[a][b]=0;
     }
    }
    return(*mat);
    }

   int* zeraVetor(int vet[NUM_LIN_DISTANCIAS], int numLinha)
   {
    int c;
       for (c=0;c<numLinha;c++){
       	vet[c]=0;
    }
    return(vet);
    }

	void imprimeVetor(int vet[NUM_LIN_DISTANCIAS], int numLinha)
   {
    int d;
       for (d=0;d<numLinha;d++){
       	cout << vet[d] <<" ";
    }
    cout << "\n";
    }

    int achou_no_vetor(int v[], int item,int tam)
    {
    int i =0;
    for (i=0;i<tam; i++){
        if (item ==v[i]) {
          return 1;
        }
    }
     return 0;
    }

    int achou_no_pedido(int mat_ped[][NUM_COL_PEDIDOS], int item,int tam)
    {
    int i =0;
    for (i=0;i<tam; i++){
        if (item ==mat_ped[i][2]) {
          return 1;
        }
    }
     return 0;
    }
    int busca_quant_pedido(int mat_ped[][NUM_COL_PEDIDOS], int local,int tam)
    {
    int i =0;
    int total=0;
    for (i=0;i<tam; i++){

        if (local ==mat_ped[i][1]) {
          total = total + mat_ped[i][2];
        }
    }
     return total;
    }

  void imprime_matriz_distancia(int mat[][NUM_COL_DISTANCIAS], int lin, int col)
  {
    int i,j;

    for (i=0;i<lin; i++){
            for (j=0;j<col; j++){
                 cout << mat[i][j] <<" ";
       }
       cout << "\n";
      }
cout << "\n";
   }

void imprime_matriz_pedidos(int mat[][NUM_COL_PEDIDOS], int lin, int col)
  {
    int i,j;

    for (i=0;i<lin; i++){
            for (j=0;j<col; j++){
                 cout << mat[i][j] <<" ";
       }
       cout << "\n";
      }
      cout << "\n";
   }

void imprime_matriz_veiculos(int mat[][NUM_COL_VEICULOS], int lin, int col)
  {
    int i,j;

    for (i=0;i<lin; i++){
            for (j=0;j<col; j++){
                 cout << mat[i][j] <<" ";
       }
       cout << "\n";
      }
      cout << "\n";
   }

   void imprime_matriz_produtos(string mat[][NUM_COL_PRODUTOS], int lin, int col)
  {
    int i,j;

    for (i=0;i<lin; i++){
            for (j=0;j<col; j++){
                 cout << mat[i][j] <<" ";
       }
       cout << "\n";
      }
      cout << "\n";
   }

    void imprime_matriz_locais(string mat[][NUM_COL_LOCAIS], int lin, int col)
  {
    int i,j;

    for (i=0;i<lin; i++){
            for (j=0;j<col; j++){
                 cout << mat[i][j] <<" ";
       }
       cout << "\n";
      }
   }

  void imprime_matriz_rotas(int mat[][NUM_LIN_DISTANCIAS], int lin, int col)
  {
    int i,j;

    for (i=0;i<lin; i++){
            for (j=0;j<col; j++){
                 cout << mat[i][j] <<" ";
       }
       cout << "\n";
      }
   }

  void prenche_indice_matriz_rotas(int mat[][NUM_LIN_DISTANCIAS], int lin, int col)
  {
    int i,j;

    for (i=0;i<lin; i++){
    	 mat[i][0]=i;
    }

    for (j=0;j<col; j++){
    	 mat[0][j]=j;
     }
   }

  void imprime_resultados( int mat_pedido [][NUM_COL_PEDIDOS],int mat_veic_rota [][NUM_LIN_DISTANCIAS],
                                 string mat_loc[][NUM_COL_LOCAIS],int quant_veic, int lin, int col)
    {
     int i,j;

     for (i=1;i<=quant_veic; i++){

         cout << " VEICULO: "<<i<<" DISTANCIA: " << mat_veic_rota[i][NUM_LIN_LOCAIS]<<"\n";
            for (j=1;j<col; j++){
                if (mat_veic_rota[i][j]>0){
                 cout << mat_veic_rota[i][j] <<" - "<<mat_loc[mat_veic_rota[i][j]-1][1] <<" \n";
                }

       }
       cout << "\n";
      }
   }
 };

 class Processamento{

 public:
	Processamento(void) //método construtor
    {
      	//
    }
    ~Processamento(void) //Destrutor. Chamado quando o objeto é liberado
     {
      //
     }

int calcula_rota(int mat_dist[][NUM_COL_DISTANCIAS],int mat_pedido [][NUM_COL_PEDIDOS],int mat_veic_rota [][NUM_LIN_DISTANCIAS], int mat_rota [][NUM_LIN_DISTANCIAS],int vet_rota[], int lin, int col)
  {
    int i ,j,r,k=1;
    int ind_menor=1, menor;
    int ind_rota=1;
    int ind=1;
    int veiculo = 1;
    int quant_carga=0;
    int lin_mat_rota=1;
    int col_mat_rota=1;
    int achou;
    int distancia_veiculo;

    ManipulaArray   m;

    for (	r=0;r<lin; r++) {
     mat_rota[mat_dist[r][0] ][    mat_dist[r][1] ] = mat_dist[r][2];
     }
    vet_rota[1]=1; ind_rota ++;
    mat_veic_rota[lin_mat_rota][col_mat_rota]=ind_menor;col_mat_rota++;

   while (k<NUM_LIN_LOCAIS){

	 menor=NUM_DISTANCIA_MAX;
     achou=0;
            for (j=1;j<NUM_LIN_LOCAIS; j++){
            if (mat_rota[ind][j] != 0 ){
          	    if (mat_rota[ind][j] < menor && m.achou_no_vetor(vet_rota,j,NUM_LIN_DISTANCIAS)==0){
	            	menor = mat_rota[ind][j];
	          	    ind_menor = j;
	          	    achou++;
		        }
		  	  }
      		}

            if (achou>0){
             if (quant_carga+m.busca_quant_pedido(mat_pedido,ind_menor,NUM_LIN_PEDIDOS)>NUM_QUANT_VEICULOS){
                lin_mat_rota++;
                col_mat_rota =1;
                mat_veic_rota[lin_mat_rota][col_mat_rota]=1;col_mat_rota++;//SEDE-CENTRO
                quant_carga=0;
			    }

			  mat_veic_rota[lin_mat_rota][col_mat_rota]=ind_menor;
			  quant_carga=quant_carga+m.busca_quant_pedido(mat_pedido,ind_menor,NUM_LIN_PEDIDOS);
			  vet_rota[ind_rota]=ind_menor; ind_rota ++;col_mat_rota++;
			  ind=ind_menor;
			  menor=NUM_DISTANCIA_MAX;
             }
           k++;

   }


   for (i=1;i<=lin_mat_rota; i++){
        distancia_veiculo=0;
       for (j=2;j<=NUM_LIN_LOCAIS; j++){
        distancia_veiculo=distancia_veiculo+mat_rota[mat_veic_rota[i][j-1]][mat_veic_rota[i][j]];
       }
       mat_veic_rota[i][NUM_LIN_LOCAIS]=distancia_veiculo;
   }
      return lin_mat_rota;
  }

};

 int main()
	{   int i=0,j=0, k=0;
	    int* v;
		int vetor_rotas[NUM_LIN_DISTANCIAS];
		int mat_distancias [NUM_LIN_DISTANCIAS][NUM_COL_DISTANCIAS];
		int mat_pedidos [NUM_LIN_PEDIDOS][NUM_COL_PEDIDOS];
		int mat_veiculos [NUM_LIN_PEDIDOS][NUM_COL_VEICULOS];
		string mat_produtos [NUM_LIN_PRODUTOS][NUM_COL_PRODUTOS];
		string mat_locais[NUM_LIN_LOCAIS][NUM_COL_LOCAIS];
		int mat_rotas [NUM_LIN_DISTANCIAS][NUM_LIN_DISTANCIAS];
		int mat_veiculo_rota [NUM_LIN_VEICULOS][NUM_LIN_DISTANCIAS];
        int teste=0;
        int quant_veiculos;
	ManipulaArquivo ma;

	string nome_arquivo_entrada;

    ma.leMatriArquivo(nome_arquivo_entrada,mat_distancias,mat_pedidos,mat_veiculos,mat_produtos,mat_locais,
	                  NUM_LIN_DISTANCIAS,	 NUM_COL_DISTANCIAS,NUM_LIN_PEDIDOS,NUM_COL_PEDIDOS,
					  NUM_LIN_VEICULOS,NUM_COL_VEICULOS,NUM_LIN_PRODUTOS,NUM_COL_PRODUTOS,NUM_LIN_LOCAIS,NUM_COL_LOCAIS);
    ManipulaArray   mr;

	mr.zeraMatrizRotas(mat_rotas,NUM_LIN_DISTANCIAS,NUM_LIN_DISTANCIAS) ;
	mr.prenche_indice_matriz_rotas(mat_rotas,NUM_LIN_DISTANCIAS,NUM_LIN_DISTANCIAS);
    mr.zeraVetor(vetor_rotas,NUM_LIN_DISTANCIAS);
    mr.zeraMatrizRotas(mat_veiculo_rota,NUM_LIN_DISTANCIAS,NUM_LIN_DISTANCIAS);

   //	cout << "DISTANCIAS: \n";
   //  mr.imprime_matriz_distancia(mat_distancias, NUM_LIN_DISTANCIAS, NUM_COL_DISTANCIAS);
   // cout << "VEICULOS: \n";
   //	mr.imprime_matriz_veiculos(mat_veiculos, NUM_LIN_VEICULOS, NUM_COL_VEICULOS);

	cout << "PRODUTOS: \n";
	mr.imprime_matriz_produtos(mat_produtos, NUM_LIN_PRODUTOS, NUM_COL_PRODUTOS);
    cout << "LOCAIS: \n";
	mr.imprime_matriz_locais(mat_locais, NUM_LIN_LOCAIS, NUM_COL_LOCAIS);
    cout << "PEDIDOS: \n";
    mr.imprime_matriz_pedidos(mat_pedidos,NUM_LIN_PEDIDOS,NUM_COL_PEDIDOS);

	Processamento pro;
	quant_veiculos=pro.calcula_rota(mat_distancias,mat_pedidos,mat_veiculo_rota,mat_rotas,vetor_rotas, NUM_LIN_DISTANCIAS, NUM_COL_DISTANCIAS);

    cout << "RESULTADOS: \n";
    mr.imprime_resultados(mat_pedidos,mat_veiculo_rota, mat_locais,quant_veiculos,NUM_LIN_LOCAIS, NUM_LIN_LOCAIS);


}