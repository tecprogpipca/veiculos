----------------------------------------------------------------------
				READ ME
-----------------------------------------------------------------------
TRABALHO DE TÉCNICAS DE PROGRAMAÇÃO
ALUNOS:
	DEBORAH LUISA DETANICO VIEIRA
	ROBERTO ARAÚJO
-----------------------------------------------------------------------
UTILIZANDO PROBLEMA DE ROTEAMENTO DE VEÍCULOS PARA UMA PIZZARIA
PROGRAMAR AS ROTAS DE ENTREGA VISANDO ECONOMIA DE TEMPO E RECURSOS.

UM ATENDENTE REGISTRA OS PEDIDOS DE PIZZA COM BAIRRO DO CLIENTE. 
COM BASE NAS LOCALIDADES DOS PEDIDOS, O SISTEMA MONTA A MELHOR
ROTA PARA AS ENTREGAS.
-----------------------------------------------------------------------
LIGUAGEM: C++
COMPILADO NO DEV-C++
-----------------------------------------------------------------------
-----------------------------------------------------------------------

ARQUIVOS NECESSÁRIOS PARA A EXECUÇÃO

veiculos.txt
PRIMEIRA LINHA CORRESPONDE ORDEM DA MATRIZ (LINHAS E COLUNAS) : 10 X 2 
COLUNAS CORRESPONDEM:
NUMERO||DESCRICAO
-----------------------------------------------------------------------

produtos.txt
PRIMEIRA LINHA CORRESPONDE ORDEM DA MATRIZ (LINHAS E COLUNAS) : 10 X 2   
NUMERO||DESCRICAO
-----------------------------------------------------------------------

locais.txt
PRIMEIRA LINHA CORRESPONDE ORDEM DA MATRIZ (LINHAS E COLUNAS) : 11 X 2   
NUMERO||DESCRICAO
-----------------------------------------------------------------------

distancias.txt
PRIMEIRA LINHA CORRESPONDE ORDEM DA MATRIZ (LINHAS E COLUNAS) : 55 X 3  
ORIGEM||DESTINO||DISTANCIA 
-----------------------------------------------------------------------

pedidos.txt
PRIMEIRA LINHA CORRESPONDE ORDEM DA MATRIZ (LINHAS E COLUNAS) : 10 X 4 
NUMERO||LOCAL||QUANTIDADE||TIPO
-----------------------------------------------------------------------